﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace chess_ui
{
    class Ucci
    {
        /// <summary>
        /// 棋子个数
        /// </summary>
        public static readonly int PieceCount = 32;

        /// <summary>
        /// fen1: rnbakabnr/9/1c5c1/p1p1p1p1p/9/9/P1P1P1P1P/1C5C1/9/RNBAKABNR => 棋子布局
        /// fen2: w => 行动方
        /// fen3: - => 固定[-]
        /// fen4: - => 固定[-]
        /// fen5: 0 => 表示双方没有吃子的走棋步数,本系统固定是[-]
        /// fen6: 1 => 表示当前的回合数
        /// </summary>
        private readonly static String fenInit = "rnbakabnr/9/1c5c1/p1p1p1p1p/9/9/P1P1P1P1P/1C5C1/9/RNBAKABNR w - - 0 1";
        public static String FEN_INIT => fenInit;

        public static Tuple<int, int> NewTuple(int x, int y)
        {
            return new Tuple<int, int>(x, y);
        }

        /// <summary>
        /// 棋盘标签名匹配坐标：9 * 10
        /// </summary>
        private static readonly Dictionary<String, Tuple<int, int>> _lbCoordinate =
            new Dictionary<String, Tuple<int, int>>()
            {
                { "lb00", NewTuple(0, 0) },  { "lb01", NewTuple(0, 1) },  { "lb02", NewTuple(0, 2) },
                { "lb03", NewTuple(0, 3) },  { "lb04", NewTuple(0, 4) },  { "lb05", NewTuple(0, 5) },
                { "lb06", NewTuple(0, 6) },  { "lb07", NewTuple(0, 7) },  { "lb08", NewTuple(0, 8) },
                { "lb10", NewTuple(1, 0) },  { "lb11", NewTuple(1, 1) },  { "lb12", NewTuple(1, 2) },
                { "lb13", NewTuple(1, 3) },  { "lb14", NewTuple(1, 4) },  { "lb15", NewTuple(1, 5) },
                { "lb16", NewTuple(1, 6) },  { "lb17", NewTuple(1, 7) },  { "lb18", NewTuple(1, 8) },
                { "lb20", NewTuple(2, 0) },  { "lb21", NewTuple(2, 1) },  { "lb22", NewTuple(2, 2) },
                { "lb23", NewTuple(2, 3) },  { "lb24", NewTuple(2, 4) },  { "lb25", NewTuple(2, 5) },
                { "lb26", NewTuple(2, 6) },  { "lb27", NewTuple(2, 7) },  { "lb28", NewTuple(2, 8) },
                { "lb30", NewTuple(3, 0) },  { "lb31", NewTuple(3, 1) },  { "lb32", NewTuple(3, 2) },
                { "lb33", NewTuple(3, 3) },  { "lb34", NewTuple(3, 4) },  { "lb35", NewTuple(3, 5) },
                { "lb36", NewTuple(3, 6) },  { "lb37", NewTuple(3, 7) },  { "lb38", NewTuple(3, 8) },
                { "lb40", NewTuple(4, 0) },  { "lb41", NewTuple(4, 1) },  { "lb42", NewTuple(4, 2) },
                { "lb43", NewTuple(4, 3) },  { "lb44", NewTuple(4, 4) },  { "lb45", NewTuple(4, 5) },
                { "lb46", NewTuple(4, 6) },  { "lb47", NewTuple(4, 7) },  { "lb48", NewTuple(4, 8) },
                { "lb50", NewTuple(5, 0) },  { "lb51", NewTuple(5, 1) },  { "lb52", NewTuple(5, 2) },
                { "lb53", NewTuple(5, 3) },  { "lb54", NewTuple(5, 4) },  { "lb55", NewTuple(5, 5) },
                { "lb56", NewTuple(5, 6) },  { "lb57", NewTuple(5, 7) },  { "lb58", NewTuple(5, 8) },
                { "lb60", NewTuple(6, 0) },  { "lb61", NewTuple(6, 1) },  { "lb62", NewTuple(6, 2) },
                { "lb63", NewTuple(6, 3) },  { "lb64", NewTuple(6, 4) },  { "lb65", NewTuple(6, 5) },
                { "lb66", NewTuple(6, 6) },  { "lb67", NewTuple(6, 7) },  { "lb68", NewTuple(6, 8) },
                { "lb70", NewTuple(7, 0) },  { "lb71", NewTuple(7, 1) },  { "lb72", NewTuple(7, 2) },
                { "lb73", NewTuple(7, 3) },  { "lb74", NewTuple(7, 4) },  { "lb75", NewTuple(7, 5) },
                { "lb76", NewTuple(7, 6) },  { "lb77", NewTuple(7, 7) },  { "lb78", NewTuple(7, 8) },
                { "lb80", NewTuple(8, 0) },  { "lb81", NewTuple(8, 1) },  { "lb82", NewTuple(8, 2) },
                { "lb83", NewTuple(8, 3) },  { "lb84", NewTuple(8, 4) },  { "lb85", NewTuple(8, 5) },
                { "lb86", NewTuple(8, 6) },  { "lb87", NewTuple(8, 7) },  { "lb88", NewTuple(8, 8) },
                { "lb90", NewTuple(9, 0) },  { "lb91", NewTuple(9, 1) },  { "lb92", NewTuple(9, 2) },
                { "lb93", NewTuple(9, 3) },  { "lb94", NewTuple(9, 4) },  { "lb95", NewTuple(9, 5) },
                { "lb96", NewTuple(9, 6) },  { "lb97", NewTuple(9, 7) },  { "lb98", NewTuple(9, 8) }
            };
        public static Dictionary<String, Tuple<int, int>> LbCoordinate => _lbCoordinate;

        /// <summary>
        /// 棋子，红色：大写，黑色：小写
        /// 帅：King，仕：Advisor，相：Bishop，马：Knight，车：Rook，炮：Cannon，兵：Pawn
        /// 
        ///        K  A  B  N  R  C  P
        /// 红色： 帅 仕 相 马 车 炮 兵
        ///        k  a  b  n  r  c  p
        /// 黑色： 将 士 象 马 车 炮 卒
        /// </summary>
        private static readonly Dictionary<char, int> _fenLbBridge = new Dictionary<char, int>()
        {
            { 'K', 0 }, { 'A', 1 }, { 'B', 3 }, { 'N', 5 }, { 'R', 7 }, { 'C', 9 }, { 'P', 11 },
            { 'k', 16 }, { 'a', 17 }, { 'b', 19 }, { 'n', 21 }, { 'r', 23 }, { 'c', 25 }, { 'p', 27 },
            { '1', 1 }, { '2', 2 },{ '3', 3 },{ '4', 4 },{ '5', 5 },{ '6', 6 },{ '7', 7 },{ '8', 8 },{ '9', 9 }
        };

        public static Dictionary<char, int> FES_LB_BRIDGE => _fenLbBridge;

        /// <summary>
        /// 棋子图像下标，黑色：16-31，红色：0-15
        /// 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
        /// 帥 士 士 相 相 馬 馬 車 車 炮 炮 兵 兵 兵 兵 兵 
        /// 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
        /// 将 仕 仕 象 象 馬 馬 車 車 炮 炮 卒 卒 卒 卒 卒
        /// </summary>
        private static readonly int[] _scalaImage = { 0, 1, 3, 5, 7, 9, 11, 16, 17, 19, 21, 23, 25, 27 };

        public static int[] ScalaImage => _scalaImage;

        /// <summary>
        /// 仕 移动可能坐标
        /// </summary>
        private static HashSet<Tuple<int, int, int, int>> _advisorLegallyMove = new HashSet<Tuple<int, int, int, int>>
        {
            new Tuple<int, int, int, int>(7,3,8,4),
            new Tuple<int, int, int, int>(7,5,8,4),
            new Tuple<int, int, int, int>(9,3,8,4),
            new Tuple<int, int, int, int>(9,5,8,4),
            new Tuple<int, int, int, int>(8,4,7,3),
            new Tuple<int, int, int, int>(8,4,7,5),
            new Tuple<int, int, int, int>(8,4,9,3),
            new Tuple<int, int, int, int>(8,4,9,5)
        };
        public static HashSet<Tuple<int, int, int, int>> AdvisorLegallyMove => _advisorLegallyMove;

        /// <summary>
        /// 相 移动可能坐标
        /// </summary>
        private static HashSet<Tuple<int, int, int, int>> _bishopLegallyMove = new HashSet<Tuple<int, int, int, int>>
        {
            new Tuple<int, int, int, int>(7,0,5,2),
            new Tuple<int, int, int, int>(7,0,9,2),
            new Tuple<int, int, int, int>(5,2,7,0),
            new Tuple<int, int, int, int>(5,2,7,4),
            new Tuple<int, int, int, int>(9,2,7,0),
            new Tuple<int, int, int, int>(9,2,7,4),
            new Tuple<int, int, int, int>(7,4,5,2),
            new Tuple<int, int, int, int>(7,4,9,2),
            new Tuple<int, int, int, int>(7,4,5,6),
            new Tuple<int, int, int, int>(7,4,9,6),
            new Tuple<int, int, int, int>(5,6,7,4),
            new Tuple<int, int, int, int>(5,6,7,8),
            new Tuple<int, int, int, int>(9,6,7,4),
            new Tuple<int, int, int, int>(9,6,7,8),
            new Tuple<int, int, int, int>(7,8,5,6),
            new Tuple<int, int, int, int>(7,8,9,6)
        };
        public static HashSet<Tuple<int, int, int, int>> BishopLegallyMove => _bishopLegallyMove;

        public static bool IsLegallyFen(String fen)
        {
            // TODO: check fen string
            return true;
        }

        /// <summary>
        /// 生成FEN字符串
        /// </summary>
        /// <param name="pieceLoc">棋子坐标</param>
        /// <returns></returns>
        public static String GenerateFen(Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            StringBuilder fen = new StringBuilder();
            int spaces;
            for (int row = 0; row < Game.ROW; row++)
            {
                if (row != 0)
                {
                    fen.Append("/");
                }
                spaces = 0;
                for (int col = 0; col < Game.COL; col++)
                {
                    var key = new Tuple<int, int>(row, col);
                    if (pieceLoc.ContainsKey(key))
                    {
                        if (spaces > 0)
                        {
                            fen.Append(String.Format("{0}", spaces));
                        }
                        var value = pieceLoc[key];
                        fen.Append(value.Item1);
                        spaces = 0;
                    }
                    else
                    {
                        spaces++;
                    }
                    if (col == 8 && spaces > 0)
                    {
                        fen.Append(String.Format("{0}", spaces));
                    }
                }
            }
            return fen.ToString();
        }

        // 用于内部转换
        public static String FenReverse(String fen)
        {
            String fen1 = fen.Split(' ')[0];
            char[] fen1CharArray = fen1.ToCharArray();
            Array.Reverse(fen1CharArray);
            for (int i = 0; i < fen1CharArray.Length; i++)
            {
                char fen1Char = fen1CharArray[i];
                if (fen1Char >= 'a' && fen1Char <= 'z')
                {
                    fen1CharArray[i] = char.ToUpper(fen1Char);
                }
                else if (fen1Char >= 'A' && fen1Char <= 'Z')
                {
                    fen1CharArray[i] = char.ToLower(fen1Char);
                }
            }
            return new String(fen1CharArray);
        }
    }
}
