﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chess_ui
{
    class Config
    {
        // 0: player red (default)
        // 1: computer red
        // 在内部数据里，电脑设置成黑色，玩家设置成红色
        private static int player = 0;
        public static int Player => player;

    }
}
