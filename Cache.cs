﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace chess_ui
{
    static class Cache
    {
        // 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
        // 帥 士 士 相 相 馬 馬 車 車 炮 炮 兵 兵 兵 兵 兵 
        // 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
        // 将 仕 仕 象 象 馬 馬 車 車 炮 炮 卒 卒 卒 卒 卒
        private static ImageWrap[] _imageWraps = new ImageWrap[Ucci.PieceCount];
        public static ImageWrap[] ImageWraps => _imageWraps;

        private static Label[,] _labelMatrix = new Label[Game.ROW, Game.COL];
        public static Label[,] LbMatrix => _labelMatrix;

        // KEY localtion x,y
        // VALUE piece,image_index
        private static Dictionary<Tuple<int, int>, Tuple<char, int>> _pieceLoc = new Dictionary<Tuple<int, int>, Tuple<char, int>>();
        public static Dictionary<Tuple<int, int>, Tuple<char, int>> PieceLoc => _pieceLoc;

        public static Dictionary<Tuple<int, int>, Tuple<char, int>> GetPieceLocBackup()
        {
            var backup = new Dictionary<Tuple<int, int>, Tuple<char, int>>();
            foreach (var item in _pieceLoc)
            {
                backup.Add(new Tuple<int, int>(item.Key.Item1, item.Key.Item2), new Tuple<char, int>(item.Value.Item1, item.Value.Item2));
            }
            return backup;
        }

        private static Image[,] _blankImages = new Image[Game.ROW, Game.COL];
        public static Image[,] BlankImages => _blankImages;

    }

    class ImageWrap
    {
        public Image image { get; set; }
        public bool IsUsing { get; set; }

        public ImageWrap(Image image)
        {
            this.image = image;
            IsUsing = false;
        }
    }

}
