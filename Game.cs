﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Windows.Controls;
using System.Net.Sockets;
using System.Net;

namespace chess_ui
{
    class Game
    {
        static String ROOT_PATH = @"D:\Work\peekaboo\chess-ui\resources\";
        // 横轴条数
        public static int ROW = 10;
        // 纵轴条数
        public static int COL = 9;
        // 棋盘上部空间
        static int Top = 20;
        // 棋盘左边空间
        static int Left = 20;
        // 格子宽度
        static int CellWidth = 50;
        // 格子高度
        static int CellHeight = 50;
        // 棋子宽度
        static int LbWidth = 45;
        // 棋子高度
        static int LbHeight = 45;
        // 棋盘线粗（内部）
        static int Inside = 1;
        // 棋盘线粗（外框）
        static int Outside = 3;
        // 兵/炮位置点缀空间
        static int LaceSpacint = (CellHeight + CellWidth) / 2 / 10;
        // 兵/炮位置点缀长度
        static int LaceLength = (CellHeight + CellWidth) / 2 / 10;
        // 棋子宽度的 1/2
        static int HalfCellHeight = CellHeight / 2;
        // 棋子高度的 1/2
        static int HalfCellWidth = CellWidth / 2;
        // 棋盘线条颜色
        static Brush LineColor = Brushes.Red;
        // 默认棋子背景颜色
        static Color ColorDefault = Colors.Transparent;
        // 选中棋子背景颜色
        static Color ColorSelected = Colors.Violet;
        // 对手棋子移动前位置背景颜色
        static Color ColorRivalMoveFrom = Colors.MediumTurquoise;
        // 对手棋子移动后位置背景颜色
        static Color ColorRivalMoveTo = Colors.MediumSpringGreen;
        // 游戏窗口
        private MainWindow win;

        //private IPAddress ip;
        //private Socket clientSocket;

        public Game(MainWindow window)
        {
            this.win = window;
        }

        public void Start()
        {
            InitBoard();
            InitPieces(Ucci.FEN_INIT);
        }

        public void InitPieces(String fen)
        {
            ClearBoard();
            if (Config.Player == 0)
            {
                // TODO
                //fen = Ucci.FenReverse(fen);
            }
            char[] fen1CharArray = fen.Trim().Split(' ')[0].ToCharArray();
            int row = 0;
            int col = 0;
            for (int i = 0; i < fen1CharArray.Length; i++)
            {
                char c = fen1CharArray[i];
                if (c == '/')
                {
                    row++;
                    col = 0;
                }
                else if (c >= '1' && c <= '9')
                {
                    col += Ucci.FES_LB_BRIDGE[c];
                }
                else
                {
                    int imageIndex = Ucci.FES_LB_BRIDGE[c];
                    while (Cache.ImageWraps[imageIndex].IsUsing == true)
                    {
                        imageIndex++;
                    }
                    SetImage(row, col, imageIndex);
                    Cache.PieceLoc.Add(new Tuple<int, int>(row, col), new Tuple<char, int>(c, imageIndex));
                    col++;
                }
            }

        }

        public void InitBoard()
        {
            win.Height = SystemParameters.WindowCaptionHeight + Top * 2.5 + CellHeight * ROW;
            win.Width = 640; // TODO
            win.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xEA, 0xE8, 0xC1));

            InitImages();
            InitLines();
            DrawLace();
            InitLabels();
        }

        void ClearBoard()
        {
            ClearStatus();

            for (int i = 0; i < Cache.ImageWraps.Length; i++)
            {
                Cache.ImageWraps[i].IsUsing = false;
            }

            for (int row = 0; row < ROW; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    ClearImage(row, col);
                }
            }
        }

        void ClearImage(int row, int col)
        {
            Label lb = Cache.LbMatrix[row, col];
            lb.Opacity = 0;
            lb.Content = Cache.BlankImages[row, col];
            lb.Background = null;
        }

        void SetImage(int row, int col, int imageIndex)
        {
            Label lb = Cache.LbMatrix[row, col];
            lb.Opacity = 1;
            lb.Content = Cache.ImageWraps[imageIndex].image;
            Cache.ImageWraps[imageIndex].IsUsing = true;
        }

        void InitLines()
        {
            // 横线
            int x1 = Left + HalfCellWidth;
            int x2 = Left + COL * CellWidth - HalfCellWidth;
            for (int row = 0; row < ROW; row++)
            {
                int y = Top + CellHeight * row + HalfCellHeight;
                if (row == 0 || row == ROW - 1)
                {
                    DrawLine(x1, x2, y, y, Outside);
                }
                else
                {
                    DrawLine(x1, x2, y, y, Inside);
                }
            }

            // 竖线
            int y1 = Top + HalfCellHeight;
            int y2 = Top + CellHeight * ROW / 2 - HalfCellHeight;
            int y3 = y2 + CellHeight;
            int y4 = Top + CellHeight * ROW - HalfCellHeight;
            for (int col = 0; col < COL; col++)
            {
                int x = Left + HalfCellWidth + col * CellHeight;
                if (col == 0 || col == COL - 1)
                {
                    DrawLine(x, x, y1, y4, Outside);
                }
                else
                {
                    DrawLine(x, x, y1, y2, Inside);
                    DrawLine(x, x, y3, y4, Inside);
                }
            }

            // 炮/兵位置点缀
            int star_x1 = Left + CellWidth * 3 + HalfCellWidth;
            int star_x2 = star_x1 + CellWidth * 2;
            int star_y1 = Top + HalfCellHeight;
            int star_y2 = star_y1 + CellHeight * 2;
            DrawLine(star_x1, star_x2, star_y1, star_y2, Inside);
            DrawLine(star_x2, star_x1, star_y1, star_y2, Inside);

            int star_y3 = star_x1 + CellHeight * 4;
            int star_y4 = star_x2 + CellHeight * 4;
            DrawLine(star_x1, star_x2, star_y3, star_y4, Inside);
            DrawLine(star_x1, star_x2, star_y4, star_y3, Inside);
        }

        void DrawLace()
        {
            for (int col = 0; col < COL; col++)
            {
                if (col == 0)
                {
                    int px = Left + HalfCellWidth;
                    int py = Top + CellHeight * 3 + HalfCellHeight;
                    DrawRightLace(px, py, 3);
                }
                else if (col == 1 || col == 7)
                {
                    int px = Left + HalfCellWidth + CellWidth * col;
                    int py = Top + CellHeight * 2 + HalfCellHeight;
                    DrawLeftLace(px, py, 5);
                    DrawRightLace(px, py, 5);
                }
                else if (col == COL - 1)
                {
                    int px = Left + CellWidth * COL - HalfCellWidth;
                    int py = Top + CellHeight * 3 + HalfCellHeight;
                    DrawLeftLace(px, py, 3);
                }
                else if (col % 2 == 0)
                {
                    int px = Left + HalfCellWidth + CellWidth * col;
                    int py = Top + CellHeight * 3 + HalfCellHeight;
                    DrawLeftLace(px, py, 3);
                    DrawRightLace(px, py, 3);
                }
            }
        }

        void DrawLeftLace(int px, int py, int jumps)
        {
            DrawLine(px - LaceSpacint, px - LaceSpacint - LaceLength, py - LaceSpacint, py - LaceSpacint, Inside);
            DrawLine(px - LaceSpacint, px - LaceSpacint, py - LaceSpacint, py - LaceSpacint - LaceLength, Inside);
            DrawLine(px - LaceSpacint, px - LaceSpacint - LaceLength, py + LaceSpacint, py + LaceSpacint, Inside);
            DrawLine(px - LaceSpacint, px - LaceSpacint, py + LaceSpacint, py + LaceSpacint + LaceLength, Inside);

            py += CellHeight * jumps;
            DrawLine(px - LaceSpacint, px - LaceSpacint - LaceLength, py - LaceSpacint, py - LaceSpacint, Inside);
            DrawLine(px - LaceSpacint, px - LaceSpacint, py - LaceSpacint, py - LaceSpacint - LaceLength, Inside);
            DrawLine(px - LaceSpacint, px - LaceSpacint - LaceLength, py + LaceSpacint, py + LaceSpacint, Inside);
            DrawLine(px - LaceSpacint, px - LaceSpacint, py + LaceSpacint, py + LaceSpacint + LaceLength, Inside);
        }

        void DrawRightLace(int px, int py, int jumps)
        {
            DrawLine(px + LaceSpacint, px + LaceSpacint + LaceLength, py - LaceSpacint, py - LaceSpacint, Inside);
            DrawLine(px + LaceSpacint, px + LaceSpacint, py - LaceSpacint, py - LaceSpacint - LaceLength, Inside);
            DrawLine(px + LaceSpacint, px + LaceSpacint + LaceLength, py + LaceSpacint, py + LaceSpacint, Inside);
            DrawLine(px + LaceSpacint, px + LaceSpacint, py + LaceSpacint, py + LaceSpacint + LaceLength, Inside);

            py += CellHeight * jumps;
            DrawLine(px + LaceSpacint, px + LaceSpacint + LaceLength, py - LaceSpacint, py - LaceSpacint, Inside);
            DrawLine(px + LaceSpacint, px + LaceSpacint, py - LaceSpacint, py - LaceSpacint - LaceLength, Inside);
            DrawLine(px + LaceSpacint, px + LaceSpacint + LaceLength, py + LaceSpacint, py + LaceSpacint, Inside);
            DrawLine(px + LaceSpacint, px + LaceSpacint, py + LaceSpacint, py + LaceSpacint + LaceLength, Inside);
        }

        void DrawLine(int x1, int x2, int y1, int y2, int strokeThickness)
        {
            Line line = new Line();
            line.Stroke = LineColor;
            line.X1 = x1;
            line.X2 = x2;
            line.Y1 = y1;
            line.Y2 = y2;
            line.StrokeThickness = strokeThickness;
            win.board.Children.Add(line);
        }

        void InitImages()
        {
            int pointer = 0;
            for (int i = 0; i < Cache.ImageWraps.Length; i++)
            {
                if (i != 0 && Ucci.ScalaImage.Contains(i))
                {
                    pointer++;
                }
                BitmapImage bitmapImage = new BitmapImage(new Uri(String.Format("{0}{1}.gif", ROOT_PATH, pointer)));
                Cache.ImageWraps[i] = new ImageWrap(new Image { Source = bitmapImage, VerticalAlignment = VerticalAlignment.Stretch });
            }

            for (int row = 0; row < ROW; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    BitmapImage bitmapImage = new BitmapImage(new Uri(String.Format("{0}99.gif", ROOT_PATH)));
                    Cache.BlankImages[row, col] = new Image { Source = bitmapImage, VerticalAlignment = VerticalAlignment.Stretch };
                }
            }
        }

        void InitLabels()
        {
            for (int row = 0; row < ROW; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    Label lb = new Label
                    {
                        Name = string.Format("lb{0}{1}", row, col),
                        Margin = new Thickness(Left + col * CellWidth, Top + row * CellHeight, 0, 0),
                        Width = LbWidth,
                        Height = LbHeight,
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xEA, 0xE8, 0xC1)),
                        Opacity = 1,
                        Visibility = Visibility.Visible,
                        BorderBrush = new SolidColorBrush(ColorDefault),
                        Content = Cache.BlankImages[row, col],
                        HorizontalContentAlignment = HorizontalAlignment.Stretch,
                        VerticalContentAlignment = VerticalAlignment.Stretch,
                    };
                    lb.MouseLeftButtonDown += new MouseButtonEventHandler(Label_MouseLeftButtonDown);
                    win.board.Children.Add(lb);
                    Cache.LbMatrix[row, col] = lb;
                }
            }
        }

        private int tagX = -1;
        private int tagY = -1;
        private char tagPiece;
        private int tagImgIndex = -1;
        private bool hasTag = false;
        IPAddress ip = IPAddress.Parse("127.0.0.1");
        private static object messageSendLocker = new object();
        private static bool pieceMoveableFlag = true;

        /// <summary>
        /// 点击棋子
        /// </summary>
        void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!pieceMoveableFlag) {
                return;
            }
            TestLog();
            Label lb = sender as Label;
            Tuple<int, int> coordinate = Ucci.LbCoordinate[lb.Name];
            int x = coordinate.Item1;
            int y = coordinate.Item2;
            char piece = '_';
            int imageIndex = -1;
            if (Cache.PieceLoc.ContainsKey(coordinate))
            {
                Tuple<char, int> pieceLoc = Cache.PieceLoc[coordinate];
                piece = pieceLoc.Item1;
                imageIndex = pieceLoc.Item2;
            }
            if (this.hasTag)
            {
                if (IsSamePiece(this.tagX, this.tagY, x, y))
                {
                    // 点中同一个棋子
                    TestLog();
                    return;
                }
                if (piece != '_')
                {
                    if (IsPlayerPiece(piece))
                    {
                        // 改变选中的棋子
                        SetPieceSelected(x, y, piece, imageIndex);
                        TestLog();
                        return;
                    }
                    else
                    {
                        // 吃子
                        if (!IsLegallyMove(this.tagX, this.tagY, x, y, this.tagPiece, MoveType.Kill, Mover.Player, Cache.PieceLoc))
                        {
                            MessageBox.Show("Not Legally Move!");
                            return;
                        }
                        var pieceLocBackup = Cache.GetPieceLocBackup();
                        if (IsPlayerKingUnderCheck(this.tagX, this.tagY, x, y, this.tagPiece, MoveType.Kill, pieceLocBackup))
                        {
                            MessageBox.Show("Not Legally Move!");
                            return;
                        }
                        KillPiece(this.tagX, this.tagY, x, y, this.tagPiece, this.tagImgIndex, Cache.PieceLoc);
                        ClearStatus();
                    }
                }
                else
                {
                    // 移动棋子
                    if (!IsLegallyMove(this.tagX, this.tagY, x, y, this.tagPiece, MoveType.Move, Mover.Player, Cache.PieceLoc))
                    {
                        MessageBox.Show("Not Legally Move!");
                        return;
                    }
                    var pieceLocBackup = Cache.GetPieceLocBackup();
                    if (IsPlayerKingUnderCheck(this.tagX, this.tagY, x, y, this.tagPiece, MoveType.Move, pieceLocBackup))
                    {
                        MessageBox.Show("Not Legally Move!");
                        return;
                    }
                    MovePiece(this.tagX, this.tagY, x, y, this.tagPiece, this.tagImgIndex, Cache.PieceLoc);
                    ClearStatus();
                }
                //Ucci.GenerateFen(Cache.PieceLoc);
                SendMessageToEngine();
            }
            else
            {
                if (piece == '_')
                {
                    // 未选中棋子
                    TestLog();
                    return;
                }
                if (IsPlayerPiece(piece))
                {
                    // 选中棋子
                    SetPieceSelected(x, y, piece, imageIndex);
                }
            }
            TestLog();
        }

        /// <summary>
        /// 标记选中棋子
        /// </summary>
        /// <param name="targetX">对象棋子x轴</param>
        /// <param name="targetY">对象棋子y轴</param>
        /// <param name="piece">对象棋子类型</param>
        /// <param name="imageIndex">对象棋子图像坐标</param>
        void SetPieceSelected(int targetX, int targetY, char piece, int imageIndex)
        {
            if (this.hasTag)
            {
                Label btn = Cache.LbMatrix[this.tagX, this.tagY];
                btn.Background = null;
            }
            this.hasTag = true;
            this.tagX = targetX;
            this.tagY = targetY;
            this.tagImgIndex = imageIndex;
            this.tagPiece = piece;
            Cache.LbMatrix[targetX, targetY].Background = new SolidColorBrush(ColorSelected);
        }

        void ClearStatus()
        {
            this.tagX = -1;
            this.tagY = -1;
            this.tagPiece = '_';
            this.tagImgIndex = -1;
            this.hasTag = false;
        }

        /// <summary>
        /// 检查是否为玩家棋子
        /// </summary>
        /// <param name="c">棋子类型</param>
        /// <returns></returns>
        bool IsPlayerPiece(char c)
        {
            return c >= 'A' && c <= 'Z';
        }

        /// <summary>
        /// 检查是否为同一个棋子
        /// </summary>
        /// <param name="srcX">起始点x轴</param>
        /// <param name="srcY">起始点y轴</param>
        /// <param name="destX">目标点x轴</param>
        /// <param name="destY">目标点y轴</param>
        /// <returns></returns>
        bool IsSamePiece(int srcX, int srcY, int destX, int destY)
        {
            return srcX == destX && srcY == destY;
        }

        /// <summary>
        /// 棋子移动
        /// </summary>
        /// <param name="srcX">起始点x轴</param>
        /// <param name="srcY">起始点y轴</param>
        /// <param name="destX">目标点x轴</param>
        /// <param name="destY">目标点y轴</param>
        /// <param name="piece">棋子</param>
        /// <param name="srcImgIndex">移动对象棋子图像坐标</param>
        void MovePiece(int srcX, int srcY, int destX, int destY, char piece, int srcImgIndex, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            ClearImage(srcX, srcY);
            SetImage(destX, destY, srcImgIndex);
            PieceLocChange(srcX, srcY, destX, destY, piece, srcImgIndex, MoveType.Move, pieceLoc);
        }

        /// <summary>
        /// 吃子
        /// </summary>
        /// <param name="srcX">起始点x轴</param>
        /// <param name="srcY">起始点y轴</param>
        /// <param name="destX">目标点x轴</param>
        /// <param name="destY">目标点y轴</param>
        /// <param name="piece">棋子</param>
        /// <param name="srcImgIndex">移动对象棋子图像坐标</param>
        void KillPiece(int srcX, int srcY, int destX, int destY, char piece, int srcImgIndex, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            ClearImage(srcX, srcY);
            ClearImage(destX, destY);
            SetImage(destX, destY, srcImgIndex);
            PieceLocChange(srcX, srcY, destX, destY, piece, srcImgIndex, MoveType.Kill, pieceLoc);
        }

        /// <summary>
        /// 棋子坐标变动
        /// </summary>
        /// <param name="srcX">起始点x轴</param>
        /// <param name="srcY">起始点y轴</param>
        /// <param name="destX">目标点x轴</param>
        /// <param name="destY">目标点y轴</param>
        /// <param name="piece">棋子</param>
        /// <param name="srcImgIndex">移动对象棋子图像坐标</param>
        /// <param name="mt">棋子移动类型</param>
        /// <param name="pieceLoc">棋子坐标</param>
        void PieceLocChange(int srcX, int srcY, int destX, int destY, char piece, int srcImgIndex, MoveType mt, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            pieceLoc.Remove(new Tuple<int, int>(srcX, srcY));
            if (mt == MoveType.Kill)
            {
                pieceLoc.Remove(new Tuple<int, int>(destX, destY));
            }
            pieceLoc.Add(new Tuple<int, int>(destX, destY), new Tuple<char, int>(piece, srcImgIndex));
        }

        /// <summary>
        /// 检查帥是否被将
        /// </summary>
        /// <returns></returns>
        bool IsPlayerKingUnderCheckPosition(Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            var playerKing = GetPlayerKingCoordinate(pieceLoc);
            foreach (var item in pieceLoc)
            {
                var key = item.Key;
                var value = item.Value;
                switch (value.Item1)
                {
                    case 'k':
                        // 将：检查是否对将
                        if (IsKingFaceOpenFile(key.Item1, key.Item2, playerKing.Item1, playerKing.Item2, pieceLoc))
                        {
                            return true;
                        }
                        break;
                    case 'n':
                    case 'r':
                    case 'c':
                    case 'p':
                        // 馬 车 炮 卒
                        if (IsLegallyMove(key.Item1, key.Item2, playerKing.Item1, playerKing.Item2, value.Item1, MoveType.Kill, Mover.Rival, pieceLoc))
                        {
                            return true;
                        }
                        break;
                    default:
                        break;
                }
            }
            return false;
        }

        /// <summary>
        /// 检查是否合法移动
        /// </summary>
        /// <param name="srcX">起始点x轴</param>
        /// <param name="srcY">起始点y轴</param>
        /// <param name="destX">目标点x轴</param>
        /// <param name="destY">目标点y轴</param>
        /// <param name="piece">棋子</param>
        /// <param name="mt">移动类型（吃子或者移动）</param>
        /// <param name="mover">行动者（玩家或者对手)</param>
        /// <returns></returns>
        bool IsLegallyMove(int srcX, int srcY, int destX, int destY, char piece, MoveType mt, Mover mover, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            char pieceUpper = char.ToUpper(piece);
            switch (pieceUpper)
            {
                case 'K':
                    // 帅
                    return destX >= 7 && destY >= 3 && destY <= 5 &&
                        (srcX == destX && (Math.Abs(srcY - destY) == 1) || srcY == destY && (Math.Abs(srcX - destX) == 1));
                case 'A':
                    // 仕
                    return Ucci.AdvisorLegallyMove.Contains(new Tuple<int, int, int, int>(srcX, srcY, destX, destY));
                case 'B':
                    // 相
                    return Ucci.BishopLegallyMove.Contains(new Tuple<int, int, int, int>(srcX, srcY, destX, destY))
                        && !pieceLoc.ContainsKey(new Tuple<int, int>((srcX + destX) / 2, (srcY + destY) / 2));
                case 'N':
                    // 马
                    int sideX = srcX - destX;
                    int sideY = srcY - destY;
                    if (Math.Abs(sideX) * Math.Abs(sideY) == 2)
                    {
                        int eyeX;
                        int eyeY;
                        if (Math.Abs(sideX) == 2)
                        {
                            eyeX = (srcX + destX) / 2;
                            eyeY = srcY;
                        }
                        else
                        {
                            eyeX = srcX;
                            eyeY = (srcY + destY) / 2;
                        }
                        return !pieceLoc.ContainsKey(new Tuple<int, int>(eyeX, eyeY));
                    }
                    else
                    {
                        return false;
                    }
                case 'R':
                    // 车
                    return CheckSameLines(srcX, srcY, destX, destY, 0, pieceLoc);
                case 'C':
                    // 炮
                    int jumps = mt == MoveType.Move ? 0 : 1;
                    return CheckSameLines(srcX, srcY, destX, destY, jumps, pieceLoc);
                case 'P':
                    // 兵
                    int min, max, step;
                    if (mover == Mover.Player)
                    {
                        min = 5; max = 6; step = 1;
                    }
                    else
                    {
                        min = 3; max = 4; step = -1;
                    }
                    if (srcX == min || srcX == max)
                    {
                        return (destY == srcY) && (srcX - destX == step);
                    }
                    else
                    {
                        return (destY == srcY) && (srcX - destX == step)
                            || (destX == srcX) && (Math.Abs(srcY - destY) == 1);
                    }
                default:
                    return false;
            }
        }

        /// <summary>
        /// 检查是否在同一条线移动
        /// </summary>
        /// <param name="srcX">起始点x轴</param>
        /// <param name="srcY">起始点y轴</param>
        /// <param name="destX">目标点x轴</param>
        /// <param name="destY">目标点y轴</param>
        /// <param name="jumps">最大跳过棋子数</param>
        /// <returns></returns>
        bool CheckSameLines(int srcX, int srcY, int destX, int destY, int jumps, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            if (srcX == destX)
            {
                return CheckSameLine(srcY, destY, true, jumps, srcX, pieceLoc);
            }
            else if (srcY == destY)
            {
                return CheckSameLine(srcX, destX, false, jumps, srcY, pieceLoc);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 检查是否在同一条线移动
        /// </summary>
        /// <param name="src">起始点</param>
        /// <param name="dest">目标点</param>
        /// <param name="orientedX">是否横向移动（x轴）</param>
        /// <param name="jumps">最大跳过棋子数</param>
        /// <param name="line">移动轴下标（当orientedX为true时，代表x轴，否则为y轴）</param>
        /// <returns></returns>
        bool CheckSameLine(int src, int dest, bool orientedX, int jumps, int line, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            int from = Math.Min(src, dest);
            int to = Math.Max(src, dest);
            int jumpsCount = 0;
            for (int i = from + 1; i < to; i++)
            {
                Tuple<int, int> key = null;
                if (orientedX)
                {
                    key = new Tuple<int, int>(line, i);
                }
                else
                {
                    key = new Tuple<int, int>(i, line);
                }
                if (pieceLoc.ContainsKey(key))
                {
                    jumpsCount++;
                    if (jumpsCount > jumps)
                    {
                        return false;
                    }
                }
            }
            return jumpsCount == jumps;
        }

        /// <summary>
        /// 检查是否对将
        /// </summary>
        /// <param name="upperX">将X坐标</param>
        /// <param name="upperY">将Y坐标</param>
        /// <param name="downX">帥X坐标</param>
        /// <param name="downY">帥Y坐标</param>
        /// <returns></returns>
        bool IsKingFaceOpenFile(int upperX, int upperY, int downX, int downY, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            if (upperY != downY)
            {
                return false;
            }
            for (int x = Math.Min(upperX, downX) + 1; x < Math.Max(upperX, downX); x++)
            {
                if (pieceLoc.ContainsKey(new Tuple<int, int>(x, upperY)))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 获取棋子帥的坐标
        /// </summary>
        /// <returns></returns>
        Tuple<int, int> GetPlayerKingCoordinate(Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            // 思考：原则上都应该有返回值，但是如果没有返回值怎么办？
            Tuple<int, int> playerKingCoordinate = null;
            foreach (var item in pieceLoc)
            {
                var value = item.Value;
                if (value.Item1 == 'K')
                {
                    var key = item.Key;
                    playerKingCoordinate = new Tuple<int, int>(key.Item1, key.Item2);
                }
            }
            return playerKingCoordinate;
        }

        /// <summary>
        /// 检查移动完以后帥是否被将军
        /// </summary>
        /// <param name="srcX">起始点x轴</param>
        /// <param name="srcY">起始点y轴</param>
        /// <param name="destX">目标点x轴</param>
        /// <param name="destY">目标点y轴</param>
        /// <param name="piece">棋子类型</param>
        /// <param name="mt">移动类型</param>
        /// <param name="pieceLoc">棋子坐标</param>
        /// <returns></returns>
        bool IsPlayerKingUnderCheck(int srcX, int srcY, int destX, int destY, char piece, MoveType mt, Dictionary<Tuple<int, int>, Tuple<char, int>> pieceLoc)
        {
            PieceLocChange(srcX, srcY, destX, destY, piece, -1, mt, pieceLoc);
            return IsPlayerKingUnderCheckPosition(pieceLoc);
        }


        /// <summary>
        /// 发送当前棋盘局势到服务器，获取电脑下一步走法
        /// </summary>
        void SendMessageToEngine()
        {

            var send_to_server = true;
            if (send_to_server)
            {
                String rcvStr = "";
                try {
                    Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    clientSocket.Connect(new IPEndPoint(ip, 8000));

                    string sendMessage = Ucci.GenerateFen(Cache.PieceLoc);
                    clientSocket.Send(Encoding.UTF8.GetBytes(sendMessage));
                    Console.WriteLine("Send：" + sendMessage);
                    byte[] rcvBytes = new byte[4]; // TODO
                    int bytes;
                    bytes = clientSocket.Receive(rcvBytes, rcvBytes.Length, 0);
                    rcvStr += Encoding.UTF8.GetString(rcvBytes, 0, bytes);
                    Console.WriteLine("Recive：{0}", rcvStr);
                    clientSocket.Close();
                } catch (Exception e) {
                    Console.WriteLine(e.ToString());
                }
                ComputerMove(rcvStr);
            }
        }

        void ComputerMove(String rcvStr) {
            if (rcvStr.Length == 4) {
                 int[] move = new int[4];
                for (int i = 0; i < 4; i++)
                {
                    move[i] = int.Parse(rcvStr[i].ToString());
                }
                Tuple<int, int> coordinateFrom = new Tuple<int, int>(move[0], move[1]);
                Tuple<char, int> pieceLocFrom = Cache.PieceLoc[coordinateFrom];
                char pieceFrom = pieceLocFrom.Item1;
                int imageIndexFrom = pieceLocFrom.Item2;
                Tuple<int, int> coordinateTo = new Tuple<int, int>(move[2], move[3]);
                if (Cache.PieceLoc.ContainsKey(coordinateTo))
                {
                    // 吃子
                    KillPiece(move[0], 
                        move[1], 
                        move[2], 
                        move[3], 
                        pieceFrom, 
                        imageIndexFrom, 
                        Cache.PieceLoc);
                } else {
                    // 移动
                    MovePiece(move[0], 
                        move[1], 
                        move[2], 
                        move[3], 
                        pieceFrom, 
                        imageIndexFrom, 
                        Cache.PieceLoc);
                }
            } else {
                MessageBox.Show("Not Legally Move!");
            }
        }

        void TestLog()
        {
            // TODO
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine(String.Format("srcX:[{0}]", this.tagX));
            Console.WriteLine(String.Format("srcY:[{0}]", this.tagY));
            Console.WriteLine(String.Format("srcPiece:[{0}]", this.tagPiece));
            Console.WriteLine(String.Format("srcImageIndex:[{0}]", this.tagImgIndex));
            Console.WriteLine(String.Format("hasSrc:[{0}]", this.hasTag));
        }
    }

    enum MoveType
    {
        Kill, Move
    }

    enum Mover
    {
        Player, Rival
    }
}
